export const apiClient = async(path, options, remote = false) => {
  const baseUrl = remote ? 'http://localhost:3001/' : 'https://pt59d0urh7.execute-api.us-east-1.amazonaws.com/dev/'
  const url = `${baseUrl}${path}`;
  const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  };
  const opts = Object.assign({}, headers, options);

  let response;

  try {
    response = await fetch(url, opts);
    const data = await response.json();
    return data;
  } catch(e) {
    console.log(response)
  }
}