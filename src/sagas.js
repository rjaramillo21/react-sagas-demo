import { fork, all } from 'redux-saga/effects';

import { sagas as Users } from './components/users';

function* rootSaga() {
  yield all([
    fork(Users),
  ]);
}

export default rootSaga;
