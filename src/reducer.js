import { combineReducers } from 'redux';

import Users from './components/users/reducer';

export default combineReducers({
  Users
});