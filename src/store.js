import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducer';
import sagas from './sagas';

const sagaMiddleWare = createSagaMiddleware();
const storeApp = createStore(reducer, applyMiddleware(sagaMiddleWare));

sagaMiddleWare.run(sagas);

export default storeApp;