import React from 'react';
import './App.css';
import Users from './components/users';

function App() {
  return (
    <div className="App">
      <h1>Demo Code</h1>
      <Users />
    </div>
  );
}

export default App;
