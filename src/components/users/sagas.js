import { takeEvery, call, put, fork } from 'redux-saga/effects'
import { apiClient } from '../../helpers/apiClient';
import { USERS_LIST, usersListSucceeded, usersListFailed } from './reducer';

// Dispatchers
const getUsers = function* (action) {
  try {
    console.log('>>>>>>>>')
    const path = 'api/users';
    const options = { method: 'GET' };
    const payload = yield call(apiClient, path, options);
    yield put(usersListSucceeded({users: payload}));
  } catch (e) {
    console.log(e);
    yield put(usersListFailed());
  }
};

// Listeners
const getUsersSaga = function* () {
  yield takeEvery(USERS_LIST, getUsers)
}

const sagas = function* () {
  yield [
    fork(getUsersSaga),
  ];
};

export default sagas;