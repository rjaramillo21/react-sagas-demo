
// import _ from 'lodash';
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { mergeProps } from '../../helpers/utils';

import { usersList } from './reducer';


const mapStateToProps = (state) => {
  return {
    users: state.Users.users,
    isFetching: state.Users.isFetching
  }
};

class Users extends Component {
  componentDidMount() {
    this.props.usersList();
  }

  render() {
    const { users, isFetching, history } = this.props;
    // const redirect = (path) => {
    //   history.push(path);
    // };

    const renderUsersList = users.map((user) => {
      return <li>user.name</li>
    })

    const renderContent = isFetching ? (<div>Fetching users...</div>) : renderUsersList;

    return (
      <div>
        <h2>User List</h2>
        <ul>{renderContent}</ul>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ usersList }, dispatch);
export default withRouter(connect(mapStateToProps, mapDispatchToProps, mergeProps)(Users));