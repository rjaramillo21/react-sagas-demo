// Actions
export const USERS_LIST = 'USERS_LIST';
export const USERS_LIST_SUCCEEDED = 'USERS_LIST_SUCCEEDED';
export const USERS_LIST_FAILED = 'USERS_LIST_FAILED';

//Creators
export const usersList = () => ({ type: USERS_LIST });
export const usersListSucceeded = payload => ({ type: USERS_LIST_SUCCEEDED, payload });
export const usersListFailed = () => ({ type: USERS_LIST_FAILED });

const initialState = {
  isFetching: false,
  users: [],
}

export default (state = initialState, { type, payload }) => {
  switch(type) {
    case USERS_LIST:
      return Object.assign({}, state , { isFetching: true });
    case USERS_LIST_SUCCEEDED:
      return Object.assign({}, state, payload, { isFetching: false });
    case USERS_LIST_FAILED:
      return Object.assign({}, state, { isFetching: false });
    default:
      return state;
  }
};